//REGISTER
import {apiBaseUrl} from "./fixeApi";

export const POST_FAKE_REGISTER = apiBaseUrl+"/api/users"
//LOGIN
export const POST_FAKE_LOGIN = apiBaseUrl+"/api/login"
export const POST_FAKE_JWT_LOGIN = apiBaseUrl+"/api/login"
export const POST_FAKE_PASSWORD_FORGET = "/fake-forget-pwd"
export const POST_FAKE_JWT_PASSWORD_FORGET = "/jwt-forget-pwd"

//PROFILE
export const POST_EDIT_JWT_PROFILE = apiBaseUrl+"/post-jwt-profile"
export const POST_EDIT_PROFILE = apiBaseUrl+"/post-fake-profile"
//${app.env.REACT_APP_API_URL}
//dashbored
export const GET_DASHBOREDS = apiBaseUrl+`/default/dashbored`
//Banque
export const GET_BANQUES = apiBaseUrl+`/banques/`
export const ADD_NEW_BANQUE = apiBaseUrl+`/api/banques`
export const UPDATE_BANQUE = apiBaseUrl+`/api/banques/`
export const DELETE_BANQUE = apiBaseUrl+`/api/banques/`

//certification
export const GET_CERTIFICATS = apiBaseUrl+"/api/certifications/"
export const ADD_NEW_CERTIFICAT = apiBaseUrl+"/api/certifications"
export const UPDATE_CERTIFICAT = apiBaseUrl+"/api/certifications/"
export const DELETE_CERTIFICAT = apiBaseUrl+"/api/certifications/"

//qualification
export const GET_QUALIFICATIONS = apiBaseUrl+"/api/qualifications/"
export const ADD_NEW_QUALIFICATION = apiBaseUrl+"/api/qualifications"
export const UPDATE_QUALIFICATION = apiBaseUrl+"/api/qualifications/"
export const DELETE_QUALIFICATION = apiBaseUrl+"/api/qualifications/"

//objet
export const GET_OBJETS = apiBaseUrl+"/api/objets"
export const ADD_NEW_OBJET= apiBaseUrl+"/api/objets"
export const UPDATE_OBJET = apiBaseUrl+"/api/objets/"
export const DELETE_OBJET = apiBaseUrl+"/api/objets/"


//Personne interne
export const GET_PERSONNES = apiBaseUrl+"/api/personnes/"
export const GET_INPERSONNES = apiBaseUrl+"/default/interne-personne/"
export const ADD_NEW_PERSONNE= apiBaseUrl+"/api/personnes"
export const UPDATE_PERSONNE = apiBaseUrl+"/api/personnes/"
export const DELETE_PERSONNE = apiBaseUrl+"/api/personnes/"

//Personne externe
export const GET_XPERSONNES = apiBaseUrl+"/default/externe-personne/"
export const ADD_NEW_XPERSONNE= apiBaseUrl+"/api/personnes"
export const UPDATE_XPERSONNE = apiBaseUrl+"/api/personnes/"
export const DELETE_XPERSONNE = apiBaseUrl+"/api/personnes/"

//All Personne
export const GET_IPERSONNES = apiBaseUrl+"/default/interne-personne/"
export const GET_AUTPERSONNES = apiBaseUrl+"/default/interne-personne/"
export const GET_SECPERSONNES = apiBaseUrl+"/default/interne-personne/"
//CHANTIER
export const GET_CHANTIERS = apiBaseUrl+"/api/chantiers/"
export const ADD_NEW_CHANTIER= apiBaseUrl+"/api/chantiers"
export const UPDATE_CHANTIER = apiBaseUrl+"/api/chantiers/"
export const DELETE_CHANTIER = apiBaseUrl+"/api/chantiers/"

//CATEGORIE
export const GET_CATEGORIES = apiBaseUrl+"/api/categories/"
export const ADD_NEW_CATEGORIE= apiBaseUrl+"/api/categories"
export const UPDATE_CATEGORIE = apiBaseUrl+"/api/categories/"
export const DELETE_CATEGORIE = apiBaseUrl+"/api/categories/"
// Groupe
export const GET_GROUPES = apiBaseUrl+"/api/groupes/"
export const ADD_NEW_GROUPE= apiBaseUrl+"/api/groupes"
export const UPDATE_GROUPE = apiBaseUrl+"/api/groupes/"
export const DELETE_GROUPE = apiBaseUrl+"/api/groupes/"

//PRESTATION
export const GET_PRESTATIONS = apiBaseUrl+"/api/prestations/"
export const ADD_NEW_PRESTATION= apiBaseUrl+"/api/prestations"
export const UPDATE_PRESTATION = apiBaseUrl+"/api/prestations/"
export const DELETE_PRESTATION = apiBaseUrl+"/api/prestations/"

//ENTREPRISE
export const GET_ENTREPRISES = apiBaseUrl+"/api/entreprises/"
export const ADD_NEW_ENTREPRISE= apiBaseUrl+"/api/entreprises"
export const UPDATE_ENTREPRISE = apiBaseUrl+"/api/entreprises/"
export const DELETE_ENTREPRISE = apiBaseUrl+"/api/entreprises/"
//ABSENCE
export const GET_ABSENCES = apiBaseUrl+"/api/absences/"
export const ADD_NEW_ABSENCE= apiBaseUrl+"/api/absences"
export const UPDATE_ABSENCE = apiBaseUrl+"/api/absences/"
export const DELETE_ABSENCE = apiBaseUrl+"/api/absences/"



//smartphone
export const GET_SMARTPHONES = apiBaseUrl+"/api/smartphones"
export const ADD_NEW_SMARTPHONE= apiBaseUrl+"/api/smartphones"
export const UPDATE_SMARTPHONE = apiBaseUrl+"/api/smartphones/"
export const DELETE_SMARTPHONE = apiBaseUrl+"/api/smartphones/"
//user
export const GET_USERS = apiBaseUrl+"/api/users"
export const ADD_NEW_USER= apiBaseUrl+"/api/users"
export const UPDATE_USER = apiBaseUrl+"/api/users/"
export const DELETE_USER = apiBaseUrl+"/api/users/"
//TARIF
export const GET_TARIFS = apiBaseUrl+"/api/tarifs/"
export const ADD_NEW_TARIF= apiBaseUrl+"/api/tarifs"
export const UPDATE_TARIF = apiBaseUrl+"/api/tarifs/"
export const DELETE_TARIF = apiBaseUrl+"/api/tarifs/"
//HOLIDAY
export const GET_HOLIDAYS = apiBaseUrl+"/api/holidays/"
export const ADD_NEW_HOLIDAY= apiBaseUrl+"/api/holidays"
export const UPDATE_HOLIDAY = apiBaseUrl+"/api/holidays/"
export const DELETE_HOLIDAY = apiBaseUrl+"/api/holidays/"


//DEMANDE
export const GET_DEMANDES = apiBaseUrl+"/api/demandes/"
export const ADD_NEW_DEMANDE= apiBaseUrl+"/api/demandes"
export const UPDATE_DEMANDE = apiBaseUrl+"/api/demandes/"
export const DELETE_DEMANDE = apiBaseUrl+"/api/demandes/"
//Contrat
export const GET_CONTRATS = apiBaseUrl+"/api/contrats/"
export const ADD_NEW_CONTRAT= apiBaseUrl+"/api/contrats"
export const UPDATE_CONTRAT = apiBaseUrl+"/api/contrats/"
export const DELETE_CONTRAT = apiBaseUrl+"/api/contrats/"

//objetRemisExtern
export const GET_EXOBJETS = apiBaseUrl+"/default/externe-objet"
export const ADD_NEW_EXOBJET= apiBaseUrl+"/api/objet_remis"
export const UPDATE_EXOBJET = apiBaseUrl+"/api/objet_remis/"
export const DELETE_EXOBJET = apiBaseUrl+"/api/objet_remis/"

//objetRemisInterne
export const GET_INOBJETS = apiBaseUrl+"/default/interne-objet"
export const ADD_NEW_INOBJET= apiBaseUrl+"/api/objet_remis"
export const UPDATE_INOBJET = apiBaseUrl+"/api/objet_remis/"
export const DELETE_INOBJET = apiBaseUrl+"/api/objet_remis/"

//remarqueExterne
export const GET_EXREMARQUES = apiBaseUrl+"/default/externe-remarque"
export const ADD_NEW_EXREMARQUE= apiBaseUrl+"/api/remarques"
export const UPDATE_EXREMARQUE = apiBaseUrl+"/api/remarques/"
export const DELETE_EXREMARQUE = apiBaseUrl+"/api/remarques/"

//remarqueInterne
export const GET_INREMARQUES = apiBaseUrl+"/default/interne-remarque"
export const ADD_NEW_INREMARQUE= apiBaseUrl+"/api/remarques"
export const UPDATE_INREMARQUE = apiBaseUrl+"/api/remarques/"
export const DELETE_INREMARQUE = apiBaseUrl+"/api/remarques/"


//evaluationExterne
export const GET_EXEVALUATIONS = apiBaseUrl+"/default/externe-evaluation"
export const ADD_NEW_EXEVALUATION= apiBaseUrl+"/api/evaluations"
export const UPDATE_EXEVALUATION = apiBaseUrl+"/api/evaluations/"
export const DELETE_EXEVALUATION = apiBaseUrl+"/api/evaluations/"

//evaluationInterne
export const GET_INEVALUATIONS = apiBaseUrl+"/default/interne-evaluation"
export const ADD_NEW_INEVALUATION= apiBaseUrl+"/api/evaluations"
export const UPDATE_INEVALUATION = apiBaseUrl+"/api/evaluations/"
export const DELETE_INEVALUATION = apiBaseUrl+"/api/evaluations/"


//Pointage
export const GET_POINTAGES = apiBaseUrl+"/api/pointages/"
export const ADD_NEW_POINTAGE = apiBaseUrl+"/api/pointages"
export const UPDATE_POINTAGE = apiBaseUrl+"/api/pointages/"
export const DELETE_POINTAGE = apiBaseUrl+"/api/pointages/"

//contact
export const GET_CONTACTS = apiBaseUrl+"/default/contacts/"
export const ADD_NEW_CONTACT= apiBaseUrl+"/api/contacts"
export const UPDATE_CONTACT = apiBaseUrl+"/api/contacts/"
export const DELETE_CONTACT = apiBaseUrl+"/api/contacts/"

//Production

export const GET_PRODUCTION = apiBaseUrl+"/production/"
export const ADD_NEW_PRODUCTION = apiBaseUrl+"/production/"
export const UPDATE_PRODUCTION = apiBaseUrl+"/production/"
export const DELETE_PRODUCTION = apiBaseUrl+"/production/"

//Activite
export const GET_ACTIVITE = apiBaseUrl+"/api/activities/"
export const ADD_NEW_ACTIVITE = apiBaseUrl+"/api/activities"
export const UPDATE_ACTIVITE = apiBaseUrl+"/api/activities/"
export const DELETE_ACTIVITE = apiBaseUrl+"/api/activities/"

//listactivite
export const GET_LISTACTIVITES = apiBaseUrl+"/api/list_activites/"
export const ADD_NEW_LISTACTIVITE = apiBaseUrl+"/api/list_activites"
export const UPDATE_LISTACTIVITE = apiBaseUrl+"/api/list_activites/"
export const DELETE_LISTACTIVITE = apiBaseUrl+"/api/list_activites/"
//personnel
export const GET_PERSONNELS = apiBaseUrl+"/api/personnels/"
export const ADD_NEW_PERSONNEL = apiBaseUrl+"/api/personnels"
export const UPDATE_PERSONNEL = apiBaseUrl+"/api/personnels/"
export const DELETE_PERSONNEL = apiBaseUrl+"/api/personnels/"
//CALENDER
export const GET_EVENTS = "/events"
export const ADD_NEW_EVENT = "/add/event"
export const UPDATE_EVENT = "/update/event"
export const DELETE_EVENT = "/delete/event"

